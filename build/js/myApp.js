$(window).ready(function() {
	var tl_1,
			power,
			powerCustom,
			bBg,
			bJar,
			bBtn,
			bLogo,
			bPromo,
			bDescr,
			bFruit,
			bSpoon,
			bSpoonHalf;

	tl_1 = new TimelineLite();
	tl_2 = new TimelineLite();
	power = 'Power0.easeNone';
	powerCustom = 'CustomEase.create("custom", "M0,0 C0.512,0.344 0.566,0.464 0.73,0.58 0.908,0.706 0.886,0.742 1,1")';
	bBg = $('.box-bg');
	bJar = $('.box-jar');
	bBtn = $('.box-btn');
	bLogo = $('.box-logo');
	bPromo = $('.box-promo');
	bDescr = $('.box-descr');
	bFruit = $('.box-fruit');
	bSpoon = $('.box-spoon');
	bSpoonHalf = $('.box-spoon-half');

	TweenMax.staggerTo([bJar, bSpoonHalf], 1.5, {opacity:1, onComplete:changeSpoon, ease: power});
	
	tl_1.to(bSpoon, 1.2, {top:-155, left:145, ease: power})
	 	.to(bSpoon, 0.8, {top:-460, left:300, rotation:-55, ease: power})
	 	.to(bSpoon, 0.8, {top:-255, left:450, rotation:-33, zIndex:7, ease: power})
	 	.to(bSpoon, 0.8, {top:0, left:505, rotation:-23, ease: power})
		.to(bSpoon, 0.8, {top:175, left:250, rotation:-10, onComplete:showBox, ease: power})
		.to(bSpoon, 0.5, {top:205, left:20, rotation:0, onComplete:addShadow, ease: power});
		
	TweenMax.to(bBg, 1.2, {boxShadow:'inset -420px -70px 700px 85px #e6e4e3', ease: powerCustom});
			
	function changeSpoon() {
		TweenMax.to(bSpoonHalf, 0.04, {opacity:0, ease: power});
		TweenMax.to(bSpoon, 0.02, {opacity:1, ease: power})
	}

	function showBox() {
		TweenMax.to(bFruit, 1, {opacity:1, ease: power})
		TweenMax.to(bBg, 0.4, {opacity:1, delay:0.4, ease: power})
		TweenMax.staggerTo([bJar, bSpoon, bFruit], 0.25, {opacity:0, delay:0.5, ease: power});
	}

	function addShadow() {
	 	tl_1.to(bBg, 1.2, {backgroundPosition:'-240px -300px', onComplete:addText, ease: power});
	}

	function addText() {
		tl_2.to(bPromo, 1.2, {top:'17vh', left:'13.7vw', opacity:1, ease: Bounce.easeOut})
				.to(bLogo, 1.2, {width:'24.55vw', height:'26vh', top:'14.5vh', right:'11.5vw', opacity:1, rotation:360, ease: Back.easeOut.config(1.7)})
				.to(bDescr, 1.2, {top:'45.6vh', right:'14.2vw', opacity:1, ease: Bounce.easeOut})
				.to(bBtn, 2, {
							top:'66vh', right:'15vw', opacity:1, rotationX:1080,
							transformOrigin:'50% 50% 30', ease: Elastic.easeOut.config(1, 0.3)
				})
				.to(bBtn, 0.6, {rotationX:360, backgroundColor:'#e6e4e3', ease: power});
	}

});
	

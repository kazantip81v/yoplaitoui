$(window).ready(function() {
	var tl_1,
			power,
			powerCustom,
			bBg,
			bJar,
			bBtn,
			bLogo,
			bPromo,
			bDescr,
			bFruit,
			bSpoon,
			bSpoonHalf;

	tl_1 = new TimelineLite();
	tl_2 = new TimelineLite();
	power = 'Power0.easeNone';
	powerCustom = 'CustomEase.create("custom", "M0,0 C0.512,0.344 0.566,0.464 0.73,0.58 0.908,0.706 0.886,0.742 1,1")';
	bBg = $('.box-bg');
	bJar = $('.box-jar');
	bBtn = $('.box-btn');
	bLogo = $('.box-logo');
	bPromo = $('.box-promo');
	bDescr = $('.box-descr');
	bFruit = $('.box-fruit');
	bSpoon = $('.box-spoon');
	bSpoonHalf = $('.box-spoon-half');

	TweenMax.staggerTo([bJar, bSpoonHalf], 1.5, {opacity:1, onComplete:changeSpoon, ease: power});
	
	tl_1.from(bSpoon, 1, {top:-136, left:132, rottion:-61.5, ease: power})
		.to(bSpoon, 0.9, {y:-330, x:155, rottion:-55, ease: power})
		.to(bSpoon, 0.8, {y:-235, x:350, rotation:-33, zIndex:7, ease: power})
	 	.to(bSpoon, 0.7, {y:0, x:400, rotation:-23, ease: power})
		.to(bSpoon, 0.7, {y:275, x:380, rotation:-10, onComplete:showBox, ease: power})
		.to(bSpoon, 0.7, {y:346, x:180, rotation:0, ease: power})
		.to(bSpoon, 0.7, {x:-113, rotation:1, onComplete:addShadow, ease: power});
		
	TweenMax.to(bBg, 1.2, {boxShadow:'inset -420px -70px 700px 85px #e6e4e3', ease: powerCustom});
			
	function changeSpoon() {
		TweenMax.to(bSpoonHalf, 0.01, {opacity:0, ease: power});
		TweenMax.to(bSpoon, 0.01, {opacity:1, ease: power});
	}

	function showBox() {
		TweenMax.to(bFruit, 1, {opacity:1, ease: power})
		TweenMax.to(bBg, 0.6, {opacity:1, delay:1.3, ease: power})
		TweenMax.staggerTo([bJar, bSpoon, bFruit], 0.4, {opacity:0, delay:1.3, ease: power});
	}

	function addShadow() {
	 	tl_1.to(bBg, 1.2, {backgroundPosition:'-270px -350px', onComplete:addText, ease: power});
	}

	function addText() {
		tl_2.fromTo(bPromo, 1.2, {y:-400, x:-400}, {y:163, x:263, opacity:1, ease: Bounce.easeOut})
				.fromTo(bLogo, 1.2, {width:'1vw', height:'1vh', y:300, x:700},
														{width:'24.55vw', height:'26vh', y:138, x:1355, opacity:1, rotation:360, ease: Back.easeOut.config(1.7)})
				.fromTo(bDescr, 1.2, {y:1435, x:1405}, {y:435, x:1405, opacity:1, ease: Bounce.easeOut})
				.fromTo(bBtn, 2, {y:900, x:-300}, {y:635, x:1405, opacity:1, rotationX:1080, transformOrigin:'50% 50% 30', ease: Elastic.easeOut.config(1, 0.3)
				})
				.to(bBtn, 0.7, {rotationX:360, transformOrigin:'50% 50%', backgroundColor:'transparent', ease: power});
	}

});
	
